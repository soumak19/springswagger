package com.SwaggerDemo.rest.webservices.restfulwebservices.RestClient;


import java.io.IOException;
import java.util.Collections;
import java.util.Date;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.SwaggerDemo.rest.webservices.restfulwebservices.user.User;
import com.SwaggerDemo.rest.webservices.restfulwebservices.user.Token;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;



import com.SwaggerDemo.rest.webservices.restfulwebservices.user.AuthRequest;
public class RestClient {
	private static final String CREATE_USER_API="http://localhost:8090/uiicDemo/createUsers";
	private static final String AUTHENTICATE_USER_API="http://localhost:8090/uiicDemo/authenticate";
	private static final String RETRIEVE_USER_API="http://localhost:8090/uiicDemo/retrieveUser/1";
	private static final String RETRIEVE_ALLUSER_API="http://localhost:8090/uiicDemo/retrieveUsers";
	static RestTemplate restTemplate=new RestTemplate();
	public static void main(String[] arg) throws JsonParseException, JsonMappingException, IOException {
		try{
			callCreateUserAPI();
		    final String jwt=callAuthenticationAPI();
			callGetAllUser(jwt);
		}
	 catch (Exception e) {
		e.printStackTrace();
	}
	}
	
	private static void callCreateUserAPI(){
	  User user=new User(4,"soumak",new Date(),"soumak","soumak123","soumak@gmail.com","9485615135");
	  RestTemplate restTemplate = new RestTemplate();
	  HttpHeaders headers = new HttpHeaders();
	  headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

	  HttpEntity<User> entity = new HttpEntity<>(user, headers);

	  ResponseEntity<User> response=restTemplate.exchange(CREATE_USER_API, HttpMethod.POST, entity, User.class);
	  System.out.println("Result - status ("+ response.getStatusCode() + ") has body: " + response.hasBody());
	}

	private static String callAuthenticationAPI(){
		AuthRequest Authuser=new AuthRequest("Ramanujan","ram123");
		  RestTemplate restTemplate = new RestTemplate();
		  HttpHeaders headers = new HttpHeaders();
		  headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

		  HttpEntity<AuthRequest> entity = new HttpEntity<>(Authuser, headers);
		 String response = restTemplate.postForObject(AUTHENTICATE_USER_API, entity, String.class);
		   return response;
		}
	private static void callGetAllUser(String jwt) throws JsonParseException, JsonMappingException, IOException{
		final ObjectMapper objectMapper = new ObjectMapper();

		JsonNode jsonNode = objectMapper.readTree(jwt);
		String Jwt = jsonNode.get("jwt").toString();
		System.out.print(Jwt);
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		headers.set("Authorization", "Bearer "+Jwt);
		HttpEntity<String> entity = new HttpEntity<String>("parameters",headers);
		ResponseEntity<String> result = restTemplate.getForEntity(RETRIEVE_ALLUSER_API, String.class,entity);
		System.out.print(result);
	}
}
