package com.SwaggerDemo.rest.webservices.restfulwebservices.user;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.SwaggerDemo.rest.webservices.restfulwebservices.user.*;


@Component
public class UserDaoService {
	@Autowired
	private UserRepository userRepository;
//	private static List<User> users = new ArrayList<>();
//
//	private static int usersCount = 3;
//
//	static {
//		users.add(new User(1, "Adam", new Date()));
//		users.add(new User(2, "Eve", new Date()));
//		users.add(new User(3, "Jack", new Date()));
//	}

	public List<User> findAll() {
		return userRepository.findAll();
	}

	public User save(User user) {
		  User users = userRepository.save(user);
		return users;
	}

	public User findOne(int id) {
		Optional<User> findUser = userRepository.findById(id);
		return findUser.get();
	}

	public User deleteById(int id) {
		userRepository.deleteById(id);
		return null;
	}

}
