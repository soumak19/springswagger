
package com.SwaggerDemo.rest.webservices.restfulwebservices.user;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description="All details about the user. ")
@Entity
public class User {

	@Id
	private Integer CustomaerId;

	@Size(min=2, message="Name should have atleast 2 characters")
	@ApiModelProperty(notes="Name should have atleast 2 characters")
	private String name;
	@Past
	@ApiModelProperty(notes="Birth date should be in the past")
	private Date birthDate;
	@ApiModelProperty(notes="Birth date should be in the past")
	@Size(min=3, message="UserName should have atleast 3 characters")
	
	private String userName;
	@ApiModelProperty(notes="Birth date should be in the past")
	@Size(min=3, message="password should have atleast 3 alphanumeric")
	
    private String password;
	@ApiModelProperty(notes="should be  provide valid email format")
    private String email;
	@ApiModelProperty(notes="should be provide valid mobile numbe")
    private String phoneNumber;
	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	protected User() {

	}

	

	

	public User(Integer customaerId, String name, Date birthDate, String userName, String password, String email, String phoneNumber) {
		super();
		CustomaerId = customaerId;
		this.name = name;
		this.birthDate = birthDate;
		this.userName = userName;
		this.password = password;
		this.email = email;
		this.phoneNumber = phoneNumber;
	}

	public Integer getCustomaerId() {
		return CustomaerId;
	}

	public void setCustomaerId(Integer customaerId) {
		CustomaerId = customaerId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	

}
