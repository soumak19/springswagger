package com.SwaggerDemo.rest.webservices.restfulwebservices.user;

public class Token {
	private String jwt;

	public Token(String jwt) {
		super();
		this.jwt = jwt;
	}

	public String getJwt() {
		return jwt;
	}

	public void setJwt(String jwt) {
		this.jwt = jwt;
	}

}
